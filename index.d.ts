import React from "react";
interface Props {
  onPermissionsDenied: () => void;
  onPictureTaken: (e: Event) => void;
  onRectangleDetect: () => void;
  // overlayColor: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  // enableTorch: PropTypes.bool,
  // useFrontCam: PropTypes.bool,
  // saturation: PropTypes.number,
  // brightness: PropTypes.number,
  // contrast: PropTypes.number,
  // detectionCountBeforeCapture: PropTypes.number,
  // detectionRefreshRateInMS: PropTypes.number,
  // quality: PropTypes.number,
  // documentAnimation: PropTypes.bool,
  // noGrayScale: PropTypes.bool,
  // manualOnly: PropTypes.bool,
}
declare class PdfScanner extends React.Component<Props, any> {
  capture: () => void;
}
export default PdfScanner;
